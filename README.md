# RecyclebotV6.2

None of these changes would be not possible without the help of PUT Chemistry group in assembling the machine. Thanks for you all, especially for one special person who won with messy cables :*

## Software

In general if you do not know anything from Arduino and Arduino C, you would need to edit only one file placed in src/Main_Code_experimental folder, and it is **config.h** file.  DO NOT CHANGE THAT LINE:

```C
#define TRUE 1 //LEAVE THAT UNCHANGED AS 1
#define FALSE 0 1 //LEAVE THAT UNCHANGED AS 
```

These are defines to make the rest of the code more readable
In there you have 2 values right now:

1. `REDUCTION` which is you gear reduction on auger stepper motor, and our team had 18:1 so we placed the the value as 18. The original RecycleBotV6 had 15:1, so in their case it should look like `#define REDUCTION 18`
2. `LCD_I2C` which is a choice between LCD with i2c converter and without. In case your LCD have i2c converter do not change anything, if you have LCD without converter just change the line to `#define LCD_I2C FALSE`

After you make the config as you with, go to Main_Code_experimental.ino file and upload it to your board by Sketch > Upload.

The pins remains in main file Main_Code_experimental.ino, and you should not change it as far as you are not confident with programming. In case you are there are some hints:

* config.h is the file with config - it is made to be as much easy to edit as it is possible
* Main_Code_experimental.ino - it is main code file - to change only for profesionals.
* screen_update.ino - it is the menu of the Recyclebot
* AccelStepper.h and AccelStepper.c are drivers for the stepper motors

Other files are to generate proper documentation for stepper motors library, there is also LICENCE file, which is only for StepperMotors library. RecycleBotV6 was on GPLv3, as well as all of the code here is.

This repository is  forked from [RecycleBot V6](https://www.appropedia.org/RepRapable_Recyclebot:_Open_source_3-D_printable_extruder_for_converting_plastic_to_3-D_printing_filament). 

## Hardware

Electrical wiring can be found in electric/scheme folder as quick.pdf file (its gonna be changed probably, so in case, just look for file with .pdf instead). 
Other folder in electric directory is for Mulier diameter sensor.

The RAMPS v1.4 symbol was taken from [matt3u github](https://github.com/matt3u/RAMPS-1.4_KiCad)

#### TODO:

- edit the scheme to include this pseudo H bridge, which reverse the motor between Spool and Puller, responsible for placing the filament on the spooler. 

## CAD 


CAD files were made in FreeCAD, and are created to be as much parametric as it is possible. In every FreeCAD file you should find a spreadsheet to make geometric changes in model parts. I would make some order in that folder, and finish the other parts in spare time.
Unfortunatly I think I made a mistake (and we also have the stall stepper motor problem, and would need to do the work again with the [Bill Dube upgrade](https://www.thingiverse.com/thing:4551843)



### Model's info

#### Finished:

* Control Panel Front

#### Partially finished:

* Winder DC Bearing Tower

#### To work on:

Make the extruder part more stiff. like [Bill Dube upgrade](https://www.thingiverse.com/thing:4551843).

- Rod End Brackets
- Control Panel Face

- the rest 




